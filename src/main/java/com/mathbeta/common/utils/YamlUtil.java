package com.mathbeta.common.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * yaml serialization and deserialization
 *
 * @author xuxiuyou
 * @date 2023/9/14 17:17
 */
public class YamlUtil {
    private static ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

    public static <T> T loadAs(InputStream is, Class<T> cls) throws IOException {
        return mapper.readValue(is, cls);
    }

    public static String dumpAsMap(Object o) throws IOException {
        return mapper.writeValueAsString(o);
    }
}
