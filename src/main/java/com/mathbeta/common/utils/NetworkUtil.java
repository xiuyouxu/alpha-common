package com.mathbeta.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.net.*;
import java.util.*;

/**
 * 网络工具类
 *
 * @author xuxiuyou
 * @date 2023/8/24 20:37
 */
public class NetworkUtil {
    public static final String BAIDU_URL = "https://www.baidu.com:443/";

    /**
     * 列出所有的物理网卡
     *
     * @return
     */
    public static List<String> listNetworkInterfaces(boolean includeWireless) {
        List<String> list = new ArrayList<>();
        ShellUtil.ExecResult result = ShellUtil.exec("ls -1 /sys/class/net/");
        String str = result.getStdout();
        if (StringUtils.isNotBlank(str)) {
            String[] s = str.trim().split("\n");
            for (String iface : s) {
                if (!includeWireless && isWireless(iface)) {
                    continue;
                }
                if ("bridge".equalsIgnoreCase(iface) ||
                        "lo".equalsIgnoreCase(iface)
                        || "docker0".equals(iface)
                        || iface.startsWith("veth")
                        || iface.contains("cni")
                        || iface.contains("br")
                        || iface.contains("flannel")) {
                    continue;
                }
                list.add(iface.trim());
            }
        }

        list.sort(Comparator.naturalOrder());
        return list;
    }

    /**
     * 列出网卡对应的ip地址（仅列出物理网卡的内网ip或外网ip，不包含loopback，虚拟网卡等）
     *
     * @return
     */
    public static Map<String, List<String>> listNetworkAddresses(boolean includeWireless) {
        List<String> names = listNetworkInterfaces(includeWireless);
        Map<String, List<String>> addressMap = new HashMap<>();
        if (null != names && !names.isEmpty()) {
            for (String name : names) {
                addressMap.put(name, getAddresses(name));
            }
        }

        return addressMap;
    }

    /**
     * 判断是否为无线网卡
     *
     * @param name
     * @return
     */
    private static boolean isWireless(String name) {
        ShellUtil.ExecResult res = ShellUtil.exec(new String[]{"/bin/sh", "-c",
                String.format("ls -1 /sys/class/net/%s | grep -E \"wireless|phy80211\" | wc -l",
                        name)});
        String count = res.getStdout();
        if (StringUtils.isNotBlank(count)) {
            return Integer.parseInt(count.trim()) > 0;
        }

        return false;
    }

    public static List<String> getAddresses(String name) {
        List<String> res = new ArrayList<>();
        ShellUtil.ExecResult result = ShellUtil.exec(new String[]{"/bin/sh", "-c",
                String.format("ip address show dev %s | grep \"inet \" | awk '{print $2}'", name)});
        String stdout = result.getStdout();
        if (StringUtils.isNotBlank(stdout)) {
            String[] lines = stdout.split("\n");
            Arrays.stream(lines).forEach(line -> {
                if (StringUtils.isNotBlank(line)) {
                    line = line.trim();
                    if (line.contains("/")) {
                        res.add(line.substring(0, line.indexOf('/')));
                    } else {
                        res.add(line);
                    }
                }
            });
        }
        return res;
    }

    /**
     * 获取第一个可访问外网的ip地址
     *
     * @return
     */
    public static Pair<NetworkInterface, InetAddress> getOSNetworkInterface() throws Exception {
        URL url = new URL(BAIDU_URL);

        Enumeration<NetworkInterface> networkInterfaceEnumeration = NetworkInterface.getNetworkInterfaces();
        while (networkInterfaceEnumeration.hasMoreElements()) {
            NetworkInterface networkInterface = networkInterfaceEnumeration.nextElement();
            if ("bridge".equals(networkInterface.getName()) || "docker0".equals(networkInterface.getName())) {
                continue;
            }
            if (networkInterface.isLoopback() || networkInterface.isVirtual() || !networkInterface.isUp()) {
                continue;
            }
            InetAddress connectedAddress = getConnectedAddress(url, networkInterface);
            if (null != connectedAddress) {
                return Pair.of(networkInterface, connectedAddress);
            }
        }
        return null;
    }

    /**
     * 获取网卡可访问外网的ip地址
     *
     * @param url
     * @param networkInterface
     * @return
     */
    private static InetAddress getConnectedAddress(URL url, NetworkInterface networkInterface) throws IOException {
        Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
        while (addresses.hasMoreElements()) {
            InetAddress address = addresses.nextElement();
            if (!InetAddress.class.isInstance(address) || !address.isSiteLocalAddress()) {
                continue;
            }
            try (Socket soc = new Socket()) {
                soc.bind(new InetSocketAddress(address, 0));
                soc.connect(new InetSocketAddress(url.getHost(), url.getPort()), 3000);
                return address;
            }
        }
        return null;
    }
}
