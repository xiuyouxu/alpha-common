package com.mathbeta.common.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * shell命令工具类
 *
 * @author xuxiuyou
 * @date 2023/9/6 17:42
 */
public class ShellUtil {
    private static ExecutorService pool = Executors.newFixedThreadPool(64);

    public static ExecResult exec(String cmd) {
        return exec(cmd, 1, TimeUnit.MINUTES);
    }

    public static ExecResult exec(String[] cmd) {
        return exec(cmd, 1, TimeUnit.MINUTES);
    }

    /**
     * 执行命令cmd并等待timeout超时
     *
     * @param cmd
     * @param timeout
     * @param unit
     * @return
     */
    public static ExecResult exec(String cmd, long timeout, TimeUnit unit) {
        return exec(() -> {
            try {
                return Runtime.getRuntime().exec(cmd);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }, cmd, timeout, unit);
    }

    /**
     * 执行命令cmd并等待timeout超时
     *
     * @param cmd
     * @param timeout
     * @param unit
     * @return
     */
    public static ExecResult exec(String[] cmd, long timeout, TimeUnit unit) {
        return exec(() -> {
            try {
                return Runtime.getRuntime().exec(cmd);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }, cmd, timeout, unit);
    }

    private static ExecResult exec(Supplier<Process> processSupplier, Object cmd, long timeout, TimeUnit unit) {
        int exitCode = 0;
        String stdout = null;
        String stderr = null;
        try {
            Process process = processSupplier.get();
            if (null != process) {
                Future<String> f1 = pool.submit(() -> {
                    try (InputStream is = process.getInputStream()) {
                        return IoUtil.readToString(is);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return "";
                });
                Future<String> f2 = pool.submit(() -> {
                    try (InputStream is = process.getErrorStream()) {
                        return IoUtil.readToString(is);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return "";
                });

                if (process.waitFor(timeout, unit)) {
                    exitCode = process.exitValue();
                    stdout = f1.get();
                    stderr = f2.get();
                }
            }
        } catch (Exception e) {
            exitCode = -1;
            stderr = e.getMessage();
            if (e instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            e.printStackTrace();
        }

        return ExecResult.builder()
                .exitCode(exitCode)
                .stdout(stdout)
                .stderr(stderr)
                .build();
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ExecResult {
        private int exitCode;
        private String stdout;
        private String stderr;
    }
}
