package com.mathbeta.common.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * 签名工具类
 *
 * @author xuxiuyou
 * @date 2023/9/13 11:40
 */
public class SignUtil {
    /**
     * 拼接传入的字符串并计算sha256散列值
     *
     * @param params
     * @return
     */
    public static String sign(String... params) throws NoSuchAlgorithmException {
        StringBuilder sb = new StringBuilder();
        if (null != params && params.length > 0) {
            for (String p : params) {
                sb.append(p);
            }
        }

        return sha256(sb.toString());
    }

    /**
     * 计算输入字符串的sha256散列值
     *
     * @param src
     * @return
     */
    public static String sha256(String src) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(src.getBytes(StandardCharsets.UTF_8));
        return bytesToHex(md.digest());
    }

    /**
     * 将字节数组转换为16进制表示
     *
     * @param bytes
     * @return
     */
    public static String bytesToHex(byte[] bytes) {
        if (null == bytes || bytes.length == 0) {
            return "";
        }
        final StringBuilder builder = new StringBuilder();
        for (byte b : bytes) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
