package com.mathbeta.common.utils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.FileTime;

/**
 * io操作工具类
 *
 * @author xuxiuyou
 * @date 2023/9/5 14:40
 */
public class IoUtil {
    public static String readToString(File file, Charset charset) throws IOException {
        try (InputStream is = new FileInputStream(file);
             ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            int len;
            byte[] buf = new byte[1024];
            while ((len = is.read(buf)) > 0) {
                baos.write(buf, 0, len);
            }
            return baos.toString(charset.name());
        }
    }

    public static String readToString(File file) throws IOException {
        return readToString(file, StandardCharsets.UTF_8);
    }

    public static String readToString(File file, boolean trim) throws IOException {
        String s = readToString(file);
        if (null != s && trim) {
            return s.trim();
        }
        return s;
    }

    public static String readToString(InputStream is, Charset charset) throws IOException {
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            int len;
            byte[] buf = new byte[1024];
            while ((len = is.read(buf)) > 0) {
                baos.write(buf, 0, len);
            }
            return baos.toString(charset.name());
        }
    }

    public static String readToString(InputStream is) throws IOException {
        return readToString(is, StandardCharsets.UTF_8);
    }

    public static byte[] read(File file) throws IOException {
        try (InputStream is = new FileInputStream(file); ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            int len;
            byte[] buf = new byte[1024];
            while ((len = is.read(buf)) > 0) {
                baos.write(buf, 0, len);
            }
            return baos.toByteArray();
        }
    }

    public static void writeToFile(String content, File file) throws IOException {
        writeToFile(content, file, false);
    }

    public static void createDir(String dir) {
        createDir(new File(dir));
    }

    public static boolean createDir(File dir) {
        return dir.mkdirs();
    }

    public static void createNewFile(String file) throws IOException {
        createNewFile(new File(file));
    }

    public static boolean createNewFile(File file) throws IOException {
        createDir(file.getParentFile());
        if (!file.exists()) {
            return file.createNewFile();
        }
        return true;
    }

    public static void writeToFile(String content, File file, boolean append) throws IOException {
        createNewFile(file);
        try (OutputStream os = new FileOutputStream(file, append)) {
            os.write(content.getBytes(StandardCharsets.UTF_8));
        }
    }

    public static void read(long bytesToRead, InputStream is, OutputStream os) throws IOException {
        int step = 4096;
        byte[] buf = new byte[step];
        while (bytesToRead > 0) {
            if (bytesToRead < 4096) {
                buf = new byte[(int) bytesToRead];
            }
            int len = is.read(buf);
            if (len > 0) {
                os.write(buf, 0, len);
                bytesToRead -= len;
            } else {
                break;
            }
        }
    }

    public static boolean write(byte[] content, File file) throws IOException {
        createNewFile(file);
        try (OutputStream os = new FileOutputStream(file)) {
            os.write(content);
            return true;
        }
    }

    public static boolean deleteRecursively(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (null != files && files.length > 0) {
                for (File f : files) {
                    deleteRecursively(f);
                }
            }
        }
        return file.delete();
    }

    public static void copySubDir(File src, File dst) throws IOException {
        File[] files = src.listFiles();
        if (null != files && files.length > 0) {
            for (File file : files) {
                copyDir(file, dst);
            }
        }
    }

    public static void copyDir(File src, File dst) throws IOException {
        if (src.isDirectory()) {
            File target = new File(dst, src.getName());
            boolean res = target.mkdirs();

            File[] files = src.listFiles();
            if (null != files && files.length > 0) {
                for (File file : files) {
                    copyDir(file, target);
                }
            }
        } else {
            copyFile(src, new File(dst, src.getName()));
        }
    }

    public static void copyFile(File src, File dst) throws IOException {
        if (src.isFile() && src.exists()) {
            createNewFile(dst);
            try (InputStream is = new FileInputStream(src);
                 OutputStream os = new FileOutputStream(dst)) {
                byte[] buf = new byte[4096];
                int len;
                while ((len = is.read(buf)) > 0) {
                    os.write(buf, 0, len);
                }
            }
        }
    }

    /**
     * 获取文件创建时间
     *
     * @param f
     * @return
     */
    public static FileTime getCreationTime(File f) throws IOException {
        BasicFileAttributeView view = Files.getFileAttributeView(f.toPath(), BasicFileAttributeView.class);
        return view.readAttributes().creationTime();
    }
}
