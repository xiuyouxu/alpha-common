package com.mathbeta.common.utils;

import java.lang.reflect.Field;

/**
 * description
 *
 * @author xuxiuyou
 * @date 2023/10/31 10:09
 */
public class ReflectionUtil {
    /**
     * 设置cls类静态成员name的值为param
     *
     * @param cls
     * @param name
     * @param param
     */
    public static void setField(Class cls, String name, Object param)
            throws NoSuchFieldException, IllegalAccessException {
        Field field = cls.getField(name);
        field.setAccessible(true);

        field.set(null, param);
    }

    /**
     * 获取cls类静态成员name的值
     *
     * @param cls
     * @param name
     */
    public static Object getField(Class cls, String name)
            throws NoSuchFieldException, IllegalAccessException {
        Field field = cls.getField(name);
        field.setAccessible(true);

        return field.get(null);
    }
}
