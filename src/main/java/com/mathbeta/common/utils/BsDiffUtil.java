package com.mathbeta.common.utils;

import io.sigpipe.jbsdiff.ui.FileUI;

import java.io.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * <p>bsdiff差分算法工具</p>
 * <p>jbsdiff可以处理小于1GB左右的文件，但是对于更大的文件需要特殊处理</p>
 *
 * @author xuxiuyou
 * @date 2024/4/16 16:19
 */
public class BsDiffUtil {

    public static final long SPLIT_FILE_SIZE = 256 * 1024 * 1024L;

    public static void main(String[] args) throws Exception {
        File oldFile = new File("f:/745.tar.gz");
        File newFile = new File("f:/765.tar.gz");
        File patchFile = new File("f:/765-patch-from-745.gz");
        String compressor = "gz";

        long start = System.currentTimeMillis();
        diff(oldFile, newFile, patchFile, compressor);
        System.out.println(String.format("patch generated: %d", System.currentTimeMillis() - start));

        File newFilePatched = new File("f:/765-patched.tar.gz");
        start = System.currentTimeMillis();
        patch(oldFile, newFilePatched, patchFile);
        System.out.println(String.format("patch finished: %d", System.currentTimeMillis() - start));
    }

    /**
     * 采用将大文件分割为多个小文件的方法解决内存溢出问题，最后将多个patch文件合并为一个
     *
     * @param oldFile
     * @param newFile
     * @param patchFile
     * @param compressor
     */
    public static void diff(File oldFile, File newFile, File patchFile, String compressor) throws Exception {
        File workingDir = new File(String.format("/tmp/%s", SignUtil.uuid()));
        File empty = new File(workingDir, "bidiff_empty");
        IoUtil.createNewFile(empty);

        File[] oldFiles = split(workingDir, oldFile, SPLIT_FILE_SIZE);
        File[] newFiles = split(workingDir, newFile, SPLIT_FILE_SIZE);
        int m = oldFiles.length;
        int n = newFiles.length;
        int k = (m > n) ? m : n;
        File[] patchFiles = new File[k];
        CountDownLatch latch = new CountDownLatch(k);
        for (int i = 0; i < k; i++) {
            File of;
            File nf;
            if (i < oldFiles.length) {
                of = oldFiles[i];
            } else {
                of = empty;
            }
            if (i < newFiles.length) {
                nf = newFiles[i];
            } else {
                nf = empty;
            }
            File pf = new File(workingDir, String.format("patch_%d", i));
            patchFiles[i] = pf;
            new Thread(() -> {
                try {
                    FileUI.diff(of, nf, pf, compressor);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    latch.countDown();
                }
            }).start();
        }
        try {
            latch.await(30L, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (OutputStream os = new FileOutputStream(patchFile)) {
            // 分片大小
            os.write(toBytes(SPLIT_FILE_SIZE));
            // 分片数
            os.write(toBytes(k));
            for (int i = 0; i < k; i++) {
                os.write(toBytes(patchFiles[i].length()));
            }
            for (int i = 0; i < k; i++) {
                try (InputStream is = new FileInputStream(patchFiles[i])) {
                    IoUtil.read(patchFiles[i].length(), is, os);
                } catch (IOException e) {
                    System.err.println(String.format("Failed to open file input stream: %s", patchFiles[i].getAbsoluteFile()));
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            IoUtil.deleteRecursively(workingDir);
        }
    }

    public static void patch(File oldFile, File newFile, File patchFile) throws Exception {
        File workingDir = new File(String.format("/tmp/%s", SignUtil.uuid()));
        File empty = new File(workingDir, "bidiff_empty");
        IoUtil.createNewFile(empty);

        PatchInfo pi = readPatchFile(workingDir, patchFile);
        if (null != pi) {
            long splitFileSize = pi.splitFileSize;
            File[] oldFiles = split(workingDir, oldFile, splitFileSize);
            File[] patchFiles = pi.patchFiles;
            File[] newFiles = new File[patchFiles.length];

            int k = patchFiles.length;
            for (int i = 0; i < k; i++) {
                File of = empty;
                if (i < oldFiles.length) {
                    of = oldFiles[i];
                }

                File nf = new File(workingDir, SignUtil.uuid());
                newFiles[i] = nf;
                FileUI.patch(of, nf, patchFiles[i]);
            }
            try (OutputStream os = new FileOutputStream(newFile)) {
                for (int i = 0; i < k; i++) {
                    try (InputStream is = new FileInputStream(newFiles[i])) {
                        IoUtil.read(newFiles[i].length(), is, os);
                    }
                }
            }
        }
    }

    private static PatchInfo readPatchFile(File workingDir, File patchFile) {
        PatchInfo pi = new PatchInfo();
        try (InputStream is = new FileInputStream(patchFile)) {
            byte[] splitSizeBytes = new byte[8];
            is.read(splitSizeBytes);
            long splitSize = toLong(splitSizeBytes);
            pi.splitFileSize = splitSize;

            byte[] lenBytes = new byte[4];
            is.read(lenBytes);
            int len = (int) toLong(lenBytes);
            long[] sizes = new long[len];
            for (int i = 0; i < len; i++) {
                is.read(splitSizeBytes);
                sizes[i] = toLong(splitSizeBytes);
            }
            File[] patchFiles = new File[len];
            pi.patchFiles = patchFiles;
            for (int i = 0; i < len; i++) {
                File pf = new File(workingDir, SignUtil.uuid());
                try (OutputStream os = new FileOutputStream(pf)) {
                    IoUtil.read(sizes[i], is, os);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                patchFiles[i] = pf;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pi;
    }

    private static class PatchInfo {
        public long splitFileSize;
        public File[] patchFiles;
    }


    public static byte[] toBytes(int i) {
        byte[] bytes = new byte[4];
        bytes[0] = (byte) ((i >> 24) & 0xFF);
        bytes[1] = (byte) ((i >> 16) & 0xFF);
        bytes[2] = (byte) ((i >> 8) & 0xFF);
        bytes[3] = (byte) (i & 0xFF);

        return bytes;
    }

    public static byte[] toBytes(long i) {
        byte[] bytes = new byte[8];
        bytes[0] = (byte) ((i >> 56) & 0xFF);
        bytes[1] = (byte) ((i >> 48) & 0xFF);
        bytes[2] = (byte) ((i >> 40) & 0xFF);
        bytes[3] = (byte) ((i >> 32) & 0xFF);
        bytes[4] = (byte) ((i >> 24) & 0xFF);
        bytes[5] = (byte) ((i >> 16) & 0xFF);
        bytes[6] = (byte) ((i >> 8) & 0xFF);
        bytes[7] = (byte) (i & 0xFF);

        return bytes;
    }

    public static long toLong(byte[] ret) {
        long c = 0;
        int bits = 0;
        int len = ret.length - 1;
        for (int i = 0; i <= len; i++) {
            // 从最后一个字节开始，每个字节左移 i*8 位
            long a = ret[len - i] & 0xFF;
            a = a << bits;
            c |= a;
            bits += 8;
        }
        return c;
    }

    private static File[] split(File workingDir, File file, long splitFileSize) {
        long size = file.length();
        int count = (int) ((size - 1) / splitFileSize) + 1;
        File[] files = new File[count];
        try (InputStream is = new FileInputStream(file)) {
            for (int i = 0; i < count; i++) {
                files[i] = read(workingDir, is, splitFileSize);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return files;
    }

    private static File read(File workingDir, InputStream is, long fileSize) {
        File file = new File(workingDir, SignUtil.uuid());
        try (OutputStream os = new FileOutputStream(file)) {
            IoUtil.read(fileSize, is, os);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}
